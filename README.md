# MQTT客户端
基于QT的MQTT客户端，编译需要先安装QtMQTT模块。

QtMQTT是Qt Automation的一部分，开源版本不提供二进制包，需要自己编译。
Windows下编译需要使用git-bash，编译过程需要调用perl解释器。

1. 下载源码
```
git clone git://code.qt.io/qt/qtmqtt.git
```

2. 编译

执行`C:\Program Files\Git\git-bash.exe`
```
export PATH=$PATH:/C/Qt/Tools/mingw730_64/bin
cd qtmqtt
/c/Qt/5.12.4/mingw73_64/bin/qmake
mingw32-make
```

2.1 交叉编译Android版本

执行`C:\Program Files\Git\git-bash.exe`
```
cd qtmqtt
export ANDROID_NDK_ROOT=/d/qtandroid/android-ndk-r19c/ PATH=$PATH:/C/Qt/Tools/mingw730_64/bin
/c/Qt/5.12.4/android_arm64_v8a/bin/qmake
mingw32-make
```

3. 安装
```
mingw32-make install
```